/**
 * represents a mathematical permutation
*/
var vector2 = function(x, y){
	return {
		/**
		 * @cfg {Number} x
		 *
		 * X-Position
		*/
		x: x,

		/**
		 * @cfg {Number} y
		 *
		 * Y-Position
		*/
		y: y,
		
		/**
		 * test method to check if the class was loaded properly
		*/
		ping: function(){
			console.log('You shall pass mofu');
		},

		/**
		 * adds the specific vector into this one and returns a new vector as result.

		 * @param {vector2}
		*/
		add: function(vector){
			return new vector2(
				this.getX() + vector.getX(),
				this.getY() + vector.getY()
			);
		},

		/**
		 * subtracts the vector from the current one and returns the result as new vector.
		*/
		subtract: function(vector){
			return new vector2(
				this.getX() - vector.getX(),
				this.getY() - vector.getY()
			);
		},

		/**
		 * Multiplies this vector with the specified factor and retrieves a new vector.
		 *
		 * @param {Float} factor
		 * @returns {vector2} result
 		*/
		scale: function(factor){
			return new vector2(
				this.getX() * factor,
				this.getY() * factor
			);
		},

		/**
		 * calculates the distance from this vector to the specified
		 *
		 * @param {vector2} other vector
		 * @returns {Float} distance
		*/
		distanceTo: function(vector){

		},


		/**
		 * Gets X-Position of this vector
		 *
		 * @returns {Number}
		*/
		getX: function(){
			return this.x;
		},

		/**
		 * Gets Y-Position of this vector
		 *
		 * @returns {Number}
		*/
		getY: function(){
			return this.y;
		}
	};
};
