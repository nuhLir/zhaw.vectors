/**
 * represents the APP
*/
var application = function(x, y){
	return {
		/**
		 * @cfg {CanvasContext} context
		*/
		context: null,

		/**
		 * @cfg {Array} all vectors for that triangle
		*/
		triangleVectors: [],

		/**
		 * Sets the canvas context
		 *
		 * @param {CanvasContext} context
		*/
		setContext: function(context){
			this.context = context;
		},

		/**
		 * Set the triangle vectors
		 *
		 * @param {Array} vectors
		*/
		setTriangleVectors: function(vectors){
			this.triangleVectors = vectors;
		},

		/**
		 * get the triangle vectors
		*/
		getTriangleVectors: function(){
			return this.triangleVectors;
		},

		/**
		 * Gets the canvas context
		*/
		getContext: function(){
			return this.context;
		},

		/**
		 * Main Function
		*/
		run: function(){
			var triangleWidth = 800;

			var A = new vector2(0, 0),
				B = new vector2(triangleWidth, 0),
				C = new vector2(triangleWidth / 2, triangleWidth),
				p0 = new vector2(triangleWidth / 6, triangleWidth / 6),
				triangles = [A, B, C];

			this.setTriangleVectors(triangles);
			this.drawTriangle(this.getContext(), A, B, C);

			var lastPoint = p0;

			for (var i = 0; i < 50000; i++){
				var targetCornerIndex = this.getRandomInt(0, 2);
				var targetCorner = triangles[targetCornerIndex];

				lastPoint = targetCorner.subtract(lastPoint).scale(0.5).add(lastPoint);

				this.drawPoint(this.getContext(), lastPoint.getX(), lastPoint.getY());
			}
		},

		/**
		 * Returns a random integer between min (inclusive) and max (inclusive)
		 * Using Math.round() will give you a non-uniform distribution!
 		*/
		getRandomInt: function(min, max) {
    		return Math.floor(Math.random() * (max - min + 1)) + min;
		},

		/**
		 * draws point to context
		 *
		 * @param {Number} x
		 * @param {Number} y
		*/
		drawPoint: function(context, x, y){
			context.fillStyle = "red";
			context.fillRect( x, y, 1, 1 );
		},

		/**
		 * Draws triangle into the canvas
		*/
		drawTriangle: function(context, A, B, C){
			this.canvasLine(context, A.getX(), A.getY(), B.getX(), B.getY());
			this.canvasLine(context, A.getX(), A.getY(), C.getX(), C.getY());
			this.canvasLine(context, B.getX(), B.getY(), C.getX(), C.getY());
		},

		/**
		 * Draws the specific line into the Context
		 *
		 * @param {CanvasContext} context
		 * @param {Number} x0
		 * @param {Number} y0
		 * @param {Number} x1
		 * @param {Number} y1
		*/
		canvasLine: function(context, x0, y0, x1, y1){
			context.beginPath();
		    context.moveTo(x0, y0);
		    context.lineTo(x1, y1);
	      	context.strokeStyle = 'blue';
		    context.stroke();
      	}
	};
};
